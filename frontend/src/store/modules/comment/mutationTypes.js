export const SET_COMMENTS = 'setComments';
export const SET_COMMENT_IMAGE = 'setTweetImage';
export const ADD_COMMENT = 'addComment';
export const SET_COMMENT = 'editComment';
